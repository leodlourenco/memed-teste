package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

var counter int
var pingCount int
var pongCount int
var mu sync.Mutex

func handler(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	counter++
	if counter%2 == 1 {
		pingCount++
		fmt.Fprintf(w, "ping")
	} else {
		pongCount++
		fmt.Fprintf(w, "pong")
	}
	mu.Unlock()
}

func metricsHandler(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	defer mu.Unlock()

	fmt.Fprintf(w, "# HELP http_requests_total Total number of HTTP requests\n")
	fmt.Fprintf(w, "# TYPE http_requests_total counter\n")
	fmt.Fprintf(w, "http_requests_total{path=\"/\", method=\"GET\"} %d\n", counter)

	fmt.Fprintf(w, "# HELP ping_responses_total Total number of responses with 'ping'\n")
	fmt.Fprintf(w, "# TYPE ping_responses_total counter\n")
	fmt.Fprintf(w, "ping_responses_total %d\n", pingCount)

	fmt.Fprintf(w, "# HELP pong_responses_total Total number of responses with 'pong'\n")
	fmt.Fprintf(w, "# TYPE pong_responses_total counter\n")
	fmt.Fprintf(w, "pong_responses_total %d\n", pongCount)
}

func main() {
	counter = 0
	pingCount = 0
	pongCount = 0

	http.HandleFunc("/", handler)
	http.HandleFunc("/metrics", metricsHandler)

	fmt.Println("Server started at localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
